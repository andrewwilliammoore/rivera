# Rivera
A project for creating cloud infrastructure with alternative providers and minimal vendor lock-in. Currently, it provides modules for creating a Kubernetes cluster, securing a cluster with TLS and letsencrypt, and running an OpenFaas instance on the cluster. The root-level module creates the OpenFaaS instance.

## Roadmap
- Provide the configurations for more services that can be applied independently depending on a project's needs.
- Support more cloud providers, ideally allowing any of these services to be applied on any one of these providers.

## Getting Started

### Prerequisites
Requires account with DigitalOcean.
- Create a spaces (s3) API key and provide the values to environment variables below.
- Manually create a space (s3 bucket) with the name `rivera-backend` for the terraform backend
  - note the endpoint for the bucket and provide value to environment variable below

Requires a custom domain name. One registered, it will be placed in an environment variable below.

### Setup Environment
Create a git-ignored file `.envrc` with the following environment variables:
```bash
export TF_VAR_DIGITAL_OCEAN_PERSONAL_ACCESS_TOKEN=...;
export DIGITAL_OCEAN_SPACES_ACCESS_KEY=...;
export DIGITAL_OCEAN_SPACES_SECRET_KEY=...;
export DIGITAL_OCEAN_SPACES_TF_BACKEND_ENDPOINT=...;

export TF_VAR_APP_DOMAIN=...; # Root domain for the app
export TF_VAR_TLS_CERT_EMAIL=...; # Email address that will appear on the public TLS cert
export TF_VAR_ACME_SERVER_URL=https://acme-staging-v02.api.letsencrypt.org/directory # Use on non-production for a fake cert
# export TF_VAR_ACME_SERVER_URL=https://acme-v02.api.letsencrypt.org/directory # Use on production for a valid cert
export TF_VAR_CERT_ISSUER_NAME=letsencrypt-staging # Use on non-production for a fake cert
# export TF_VAR_CERT_ISSUER_NAME=letsencrypt-prod # Use on production for a valid cert

export TF_VAR_OPENFAAS_SUBDOMAIN=...; # subdomain for accessing OpenFaaS service
# Read by the faas-cli tool. Variable name is specific.
export OPENFAAS_URL=https://$OPENFAAS_SUBDOMAIN.$APP_DOMAIN

export DOCKERHUB_USERNAME=...; # Your username on DockerHub
```

### Install Local Dependencies
Install `tfenv`
Install `kubectl`

### Setup Infrastructure
```bash
./bin/apply-terraform
```

Access your OpenFaas dashboard at `https://${OPENFAAS_SUBDOMAIN}.${APP_DOMAIN}`
- username: admin
- password: retrieve from ./infrastructure/openfaas-password.txt

## Deploy Functions
Note that functions can only be deployed in the cloud when using production values for generating TLS certificates. Otherwise, only local use is possible.

Install `docker`

Deploy
```bash
./bin/deploy-function
```

Invoke from command-line
```bash
. .envrc; faas invoke rivera
```

## Teardown Infrastructure
Destroys all resources except for the terraform state backend storage
```bash
./bin/destroy-terraform
```
