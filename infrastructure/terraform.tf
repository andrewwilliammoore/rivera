terraform {
  required_version = "~> 0.12.26"

  backend "s3" {
    bucket                      = "rivera-backend"
    key                         = "terraform"
    region                      = "us-east-1" # Placeholder for API s3 compatibility
    skip_credentials_validation = true
    encrypt                     = true
  }
}
