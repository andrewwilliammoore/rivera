module "kubernetes_cluster_with_insecure_load_balancer" {
  source = "./kubernetes_cluster_with_insecure_load_balancer"
}

variable "APP_DOMAIN" {}

resource "digitalocean_domain" "openfaas" {
  name = var.APP_DOMAIN
}

locals {
  load_balancer_ip = module.kubernetes_cluster_with_insecure_load_balancer.load_balancer_ip
}

resource "digitalocean_record" "openfaas_a" {
  domain = digitalocean_domain.openfaas.name
  type   = "A"
  name   = var.OPENFAAS_SUBDOMAIN
  value  = local.load_balancer_ip

  provisioner "local-exec" {
    command = "${path.module}/bin/verify_dns ${var.OPENFAAS_SUBDOMAIN}.${digitalocean_domain.openfaas.name} ${local.load_balancer_ip}"
  }
}
