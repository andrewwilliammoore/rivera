provider "helm" {
  version = "1.2.3"

  kubernetes {
    config_path = local_file.cluster_config.filename
  }
}

resource "helm_release" "nginx_ingress" {
  depends_on = [local_file.cluster_config]

  name       = "nginx-ingress"
  repository = "https://kubernetes-charts.storage.googleapis.com/"
  chart      = "nginx-ingress"
  atomic     = true

  set {
    name  = "controller.publishService.enabled"
    value = "true"
  }
}

# This is necessary, since the helm_release resource is not fully created and ready immediately. The sleep time
# is arbitrary and can be adjusted.
resource "time_sleep" "nginx_ingress_load_balancer" {
  depends_on      = [helm_release.nginx_ingress]
  create_duration = "60s"
}

locals {
  nginx_ingress_config_file_path = "${path.module}/nginx_ingress_config.json"
}

# Write the configuration of the created ingress to a file due to the terraform state not knowing the results
# of changes applied by helm. This may change in a future version of the kubernetes provider.
# When attempting to use a local_file resource, it seemed the resource was considered to be created before
# the content had been written to the file.
resource "null_resource" "write_nginx_ingress_load_balancer_config_file" {
  depends_on = [time_sleep.nginx_ingress_load_balancer]

  provisioner "local-exec" {
    command = "${path.module}/bin/write_nginx_ingress_load_balancer_config_file ${local_file.cluster_config.filename} ${local.nginx_ingress_config_file_path}"
  }
}

data "local_file" "nginx_ingress_config" {
  # Just need the ip of the load balancer created by helm. Using a
  # a local_file resource does not work, since it relies on the output of kubectl, and the file resource is considered
  # to be created before the contents of the file can be written
  depends_on = [null_resource.write_nginx_ingress_load_balancer_config_file]
  filename   = local.nginx_ingress_config_file_path
}
