resource "digitalocean_kubernetes_cluster" "openfaas" {
  name   = "openfaas"
  region = "fra1"
  # Or grab the latest version slug from `doctl kubernetes options versions`...
  version = "1.18.6-do.0"

  node_pool {
    name       = "openfaas-autoscale-worker-pool"
    size       = "s-4vcpu-8gb" # Minimum instance size for cluster
    auto_scale = true
    min_nodes  = 1
    max_nodes  = 5
  }
}

# Save the cluster config to a file that can be read by kubectl and helm. These tools do not yet
# have the needed Terraform functionality.
resource "local_file" "cluster_config" {
  filename          = "${path.module}/kubernetes_cluster_config.yml"
  sensitive_content = digitalocean_kubernetes_cluster.openfaas.kube_config.0.raw_config
}

provider "kubernetes" {
  version     = "1.11.3"
  config_path = local_file.cluster_config.filename
}
