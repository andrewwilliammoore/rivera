output "load_balancer_ip" {
  value = jsondecode(data.local_file.nginx_ingress_config.content).status.loadBalancer.ingress[0].ip
  description = "The ip address of the load balancer for the cluster's nginx ingress"
}

output "cluster_config_file" {
  value = local_file.cluster_config
  description = "The config file generated for the kubernetes cluster"
}
