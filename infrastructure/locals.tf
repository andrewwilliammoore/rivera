locals {
  cluster_config_filename = module.kubernetes_cluster_with_insecure_load_balancer.cluster_config_file.filename
}
