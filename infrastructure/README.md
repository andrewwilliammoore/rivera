This module creates an OpenFaaS instance on Kubernetes and uses letsencrypt to secure traffic with TLS. A subdomain is created that points to the instance. Serverless functions can be deployed to it.
