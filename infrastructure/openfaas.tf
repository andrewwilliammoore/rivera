# Email address used for ACME registration
variable "TLS_CERT_EMAIL" {}

module "kubernetes_cluster_with_tls_load_balancer" {
  source = "./kubernetes_cluster_with_tls_load_balancer"

  cluster_fqdn            = digitalocean_record.openfaas_a.fqdn
  tls_cert_email          = var.TLS_CERT_EMAIL
  cluster_config_filename = local.cluster_config_filename
  acme_server_url         = var.ACME_SERVER_URL
  cert_issuer_name        = var.CERT_ISSUER_NAME
}

resource "null_resource" "apply_faas_namespaces" {
  depends_on = [module.kubernetes_cluster_with_tls_load_balancer.status_check]

  provisioner "local-exec" {
    command = "KUBECONFIG=${local.cluster_config_filename} kubectl apply -f https://raw.githubusercontent.com/openfaas/faas-netes/master/namespaces.yml"
  }
}

locals {
  openfaas_config_filename   = "${path.module}/openfaas.yml"
  openfaas_password_filename = "${path.module}/openfaas-password.txt"
  openfaas_namespace         = "openfaas"
}

resource "local_file" "openfaas_chart" {
  filename = local.openfaas_config_filename

  content = templatefile("${local.openfaas_config_filename}.template", {
    fqdn             = digitalocean_record.openfaas_a.fqdn,
    cert_secret_name = module.kubernetes_cluster_with_tls_load_balancer.cert_secret_name
    cert_issuer_name = var.CERT_ISSUER_NAME
  })
}

provider "helm" {
  version = "1.2.3"

  kubernetes {
    config_path = local.cluster_config_filename
  }
}

resource "helm_release" "faas_netes" {
  depends_on = [null_resource.apply_faas_namespaces]

  name       = "openfaas"
  repository = "https://openfaas.github.io/faas-netes/"
  version    = "6.0.1"
  values     = [local_file.openfaas_chart.content]
  chart      = "openfaas"
  namespace  = local.openfaas_namespace
  atomic     = true
}

resource "local_file" "openfaas_password" {
  depends_on = [helm_release.faas_netes]

  filename = local.openfaas_password_filename
  content  = ""

  provisioner "local-exec" {
    command = "${path.module}/bin/write_openfaas_password_file ${local.cluster_config_filename} ${local.openfaas_namespace} ${local.openfaas_password_filename}"
  }
  provisioner "local-exec" {
    command = "echo Your OpenFaaS installation is ready at $OPENFAAS_URL. Default username is admin, and the password is written to ./infrastructure/${local_file.openfaas_password.filename}"
  }
}
