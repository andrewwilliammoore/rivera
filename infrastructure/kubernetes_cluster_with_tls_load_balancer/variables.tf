variable "cluster_fqdn" {
  type = string
  description = "The domain and any subdomains under which the cluster can be accessed"
}

variable "tls_cert_email" {
  type = string
  description = "The email address to be associated with the TLS certificate"
}

variable "cluster_config_filename" {
  type = string
  description = "A file name and path for a kubernetes config file, such as what would be used for KUBECONFIG=..."
}

variable "acme_server_url" {
  type = string
  description = <<EOF
A URL for contacting ACME for generating a TLS cert.
https://acme-staging-v02.api.letsencrypt.org/directory for staging.
https://acme-v02.api.letsencrypt.org/directory for production.
EOF
}

variable "cert_issuer_name" {
  type = string
  description = <<EOF
A certificate issuer used by cert-manager for generating TLS certificates.
For example, letsencrypt-staging or letsencrypt-prod
See https://cert-manager.io/docs/ for more options and info.
EOF
}
