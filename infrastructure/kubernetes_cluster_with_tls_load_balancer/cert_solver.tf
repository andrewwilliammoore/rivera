locals {
  cert_solver_backend_name            = "acme-cert-solver-backend"
  cert_solver_backend_config_filename = "${path.module}/${local.cert_solver_backend_name}.yml"
  cert_solver_config_filename = "${path.module}/acme-cert-solver.yml"
  cert_secret_name            = "openfaas-cert"
}

resource "local_file" "cert_solver_backend_config" {
  filename = local.cert_solver_backend_config_filename

  content = templatefile("${local.cert_solver_backend_config_filename}.template", {
    cert_solver_backend_name = local.cert_solver_backend_name
  })
}

resource "null_resource" "apply_cert_solver_backend" {
  provisioner "local-exec" {
    command = "KUBECONFIG=${var.cluster_config_filename} kubectl apply -f ${local.cert_solver_backend_config_filename}"
  }
}

resource "null_resource" "verify_cert_solver_backend" {
  provisioner "local-exec" {
    command = "${path.module}/bin/verify_cert_solver_backend ${var.cluster_config_filename} ${local.cert_solver_backend_name}"
  }
}

resource "local_file" "cert_solver_config" {
  filename = local.cert_solver_config_filename

  content = templatefile("${local.cert_solver_config_filename}.template", {
    fqdn                     = var.cluster_fqdn,
    cert_secret_name         = local.cert_secret_name
    cert_solver_backend_name = local.cert_solver_backend_name
  })
}

resource "null_resource" "apply_cert_solver" {
  depends_on = [null_resource.verify_cert_solver_backend]

  provisioner "local-exec" {
    command = "KUBECONFIG=${var.cluster_config_filename} kubectl apply -f ${local_file.cert_solver_config.filename}"
  }
}

resource "null_resource" "verify_cert_solver_http" {
  depends_on = [null_resource.apply_cert_solver]

  provisioner "local-exec" {
    command = "${path.module}/bin/verify_cert_solver_http ${var.cluster_fqdn}"
  }
}
