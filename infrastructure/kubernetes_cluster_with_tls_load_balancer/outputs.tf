output "cert_secret_name" {
  value = local.cert_secret_name
  description = "The name of the kubernetes cluster secret that holds the certificate"
}

output "status_check" {
  value = null_resource.delete_temporary_backend.id
  description = "An arbitrary value that is the id of the last null resource provisioner run for this module. This is to declare dependencies."
}
