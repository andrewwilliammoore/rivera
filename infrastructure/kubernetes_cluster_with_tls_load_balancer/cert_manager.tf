locals {
  cert_manager_namespace            = "cert-manager"
  cert_issuer_config_filename = "${path.module}/cert_issuer.yml"
}

resource "helm_release" "cert_manager" {
  depends_on = [null_resource.verify_cert_solver_http]

  name       = "cert-manager"
  repository = "https://charts.jetstack.io"
  version    = "v0.15.1"
  chart      = "cert-manager"
  atomic     = true

  set {
    name  = "installCRDs"
    value = "true"
  }
}

resource "local_file" "cert_issuer_config" {
  depends_on = [helm_release.cert_manager]
  filename   = local.cert_issuer_config_filename

  content = templatefile("${local.cert_issuer_config_filename}.template", {
    cert_email       = var.tls_cert_email
    cert_secret_name = local.cert_secret_name
    acme_server_url  = var.acme_server_url
    cert_issuer_name = var.cert_issuer_name
  })

  provisioner "local-exec" {
    command = "KUBECONFIG=${var.cluster_config_filename} kubectl apply -f ${local_file.cert_issuer_config.filename}"
  }
}

resource "null_resource" "delete_temporary_backend" {
  depends_on = [local_file.cert_issuer_config]

  provisioner "local-exec" {
    command = "KUBECONFIG=${var.cluster_config_filename} kubectl delete -f ${local_file.cert_solver_backend_config.filename}"
  }
  provisioner "local-exec" {
    command = "KUBECONFIG=${var.cluster_config_filename} kubectl delete -f ${local_file.cert_solver_config.filename}"
  }
}
