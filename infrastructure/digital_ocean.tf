variable "DIGITAL_OCEAN_PERSONAL_ACCESS_TOKEN" {}

provider "digitalocean" {
  version = "~> 1.20"
  token   = var.DIGITAL_OCEAN_PERSONAL_ACCESS_TOKEN
}
